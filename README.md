# Liquid Democracy Poll

A polling app using a [liquid democracy](https://en.wikipedia.org/wiki/Liquid_democracy) model.

## Notes
- [Discovery](./docs/notes/Discovery.md)
- [Design](./docs/notes/Design.md)