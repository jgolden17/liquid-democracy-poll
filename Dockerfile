FROM node:latest
WORKDIR /app
COPY package.json /app
RUN npm install -g nodemon
RUN npm install
RUN ls
COPY . /app
EXPOSE 4000
CMD ["nodemon", "app.js"]