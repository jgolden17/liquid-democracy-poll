# Discovery

## What is Liquid Democracy?

- A form of delegative democracy
- Utilizes elements of both direct and representative democracy

## What are the features of Liquid Democracy?

- Voters have the right to vote directly on all policy issues
- Voters have the option to delegate their votes to someone who will vote on their behalf
- Any individual may be delegated votes to a proxy
- Proxies may delegate their vote as well as any votes they have been delegated by others resulting in "metadelegation"
- Delegation of votes may be absolute, policy-specific, time-sensitive, or not utilized by voters
- Voters have the right to revoke their vote delegation at any time
- In policy specific delegation, voters may also select different delegates for different issues

## Are there already tools that implement a liquid voting model?  
Yes. _damn_.

- [LiquedFeedback](https://liquidfeedback.com/en/)

### Can I do it better?

Probably not.

### Then why do it?

Cuz why not?  It'll be a fun exercise in Domain Driven Development.