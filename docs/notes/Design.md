# Design

## Use Cases

<img src='./Liquid Democracy Use Case Diagram.png'>

<br />
<br />

## Entities

<img src='./Liquid Democracy ERD.png'>

<br />
<br />

> The above diagrams didn't really tell me enough so I tried event storming...

## Event Storming

<img src='./Liquid Democracy Event Storm.png'>

<br />
<br />

### Context Mapping

<img src='./Liquid Democracy Context Mapping.png'>
