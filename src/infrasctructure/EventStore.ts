import { Db, ObjectId } from 'mongodb';
import PubSub from 'pubsub-js';
import { AggregateRoot } from '../core/AggregateRoot';

export interface IEventStore {
  save(aggregate: AggregateRoot, createSnapshot?: boolean): Promise<void>;
  load<T>(
    aggregateId: string,
    aggregateType: new (id: string) => T
  ): Promise<AggregateRoot>;
}

type EventStoreConfig = {
  database: Db;
};

export class EventStore implements IEventStore {
  private database: Db;

  constructor({ database }: EventStoreConfig) {
    this.database = database;
  }

  async save(aggregate: AggregateRoot, createSnapshot = false): Promise<void> {
    const { changes } = aggregate;

    if (changes.length === 0) {
      return;
    }

    const streamId = this.getStreamId(aggregate);

    await this.database
      .collection(this.getEventCollectionName(aggregate))
      .insertMany(
        changes.map(change => ({
          streamId,
          type: change.type,
          data: change.data,
          timestamp: change.timestamp,
        }))
      );

    if (createSnapshot) {
      await this.database
        .collection(this.getSnapshotCollectionName(aggregate))
        .insertOne({
          ...aggregate,
          streamId,
          createdAt: new Date(),
        });
    }

    PubSub.publish(this.getEventName(aggregate), aggregate);
  }

  async load<T>(aggregateId: string, aggregateType: new (id: string) => T): Promise<AggregateRoot> {
    const aggregate = <AggregateRoot>(<unknown>new aggregateType(aggregateId));

    const streamId = this.getStreamId(aggregate);

    const eventCollectionName = this.getEventCollectionName(aggregate);

    const snapshotCollectionName = this.getSnapshotCollectionName(aggregate);

    const maybeSnapshot = await this.database
      .collection(snapshotCollectionName)
      .findOne({ streamId }, { sort: { createdAt: -1 } });

    if (maybeSnapshot) {
      aggregate.loadFromSnapshot(<unknown>maybeSnapshot);
    }

    const events = await this.database
      .collection(eventCollectionName)
      .find({ streamId })
      .toArray();

    aggregate.load(
      events.map(event => ({
        id: event.streamid,
        type: event.type,
        data: event.data,
        timestamp: event.timestamp,
      }))
    );

    return aggregate;
  }

  getEventCollectionName(aggregate: AggregateRoot): string {
    return `${aggregate.constructor.name.toLowerCase()}-events`;
  }

  getSnapshotCollectionName(aggregate: AggregateRoot): string {
    return `${aggregate.constructor.name.toLowerCase()}-snapshhots`;
  }

  getEventName(aggregate: AggregateRoot): string {
    return `${aggregate.constructor.name.toUpperCase()}_PERSISTED`;
  }

  getStreamId(aggregate: AggregateRoot): ObjectId {
    return new ObjectId(aggregate.id);
  }
}
