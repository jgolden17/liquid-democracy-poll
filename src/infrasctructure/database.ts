import { Db, MongoClient } from 'mongodb';

async function loadDatabase(): Promise<Db> {
  const client = new MongoClient('mongodb://localhost:27017');

  await client.connect();

  return client.db();
}

export default loadDatabase;
