import { UseCase, UseCaseFactory } from '../../core';
import { IResolutionRepository } from '../application/ResolutionRepository';
import { ResolutionDetails } from '../domain/Resolution';

export type GetResolutionResponse = ResolutionDetails[];

export type GetResolutionsUseCase = UseCase<void, GetResolutionResponse>;

type Dependencies = {
  resolutionRepository: IResolutionRepository;
}

const getResolutions: UseCaseFactory<Dependencies, void, GetResolutionResponse> = (config: Dependencies) => {
  const { resolutionRepository } = config;

  return async () => {
    console.log('Execute Get Resolutions Use Case start');
    try {
      const resolutions = await resolutionRepository.find();

      return resolutions.map((resolution) => ({
        id: resolution.id,
        title: resolution.title,
        proposal: resolution.proposal,
        proposedBy: resolution.proposedBy,
        status: resolution.status,
        tabledAt: resolution.tabledAt,
        tabledReason: resolution.tabledReason,
        passedAt: resolution.passedAt,
        failedAt: resolution.failedAt,
        comments: resolution.comments,
      }));
    } catch (error) {
      console.log(error);
      throw error;
    } finally {
      console.log('Execute Get Resolutions Use Case end');
    }
  };
};

export default getResolutions;
