import { ObjectId } from 'mongodb';
import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Resolution } from '../domain/Resolution';

export type ProposeResolutionRequest = {
  title: string;
  proposal: string;
  proposedBy: string;
};

export type ProposeResolutionUseCase = UseCase<ProposeResolutionRequest, string>;

type Dependencies = {
  eventStore: IEventStore;
}

const proposeResolution: UseCaseFactory<Dependencies, ProposeResolutionRequest, string> = (config: Dependencies) => {
  const { eventStore } = config;

  return async (request): Promise<string> => {
    const resolutionDetails = {
      id: new ObjectId().toHexString(),
      title: request.title,
      proposal: request.proposal,
      proposedBy: request.proposedBy,
    };

    const resolution = Resolution.create(resolutionDetails);

    await eventStore.save(resolution);

    return resolution.id;
  };
};

export default proposeResolution;
