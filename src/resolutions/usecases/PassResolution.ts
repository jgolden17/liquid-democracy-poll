import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Resolution } from '../domain/Resolution';

export type PassResolutionRequest = {
  id: string;
};

export type PassResolutionUseCase = UseCase<PassResolutionRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const passResolution: UseCaseFactory<Dependencies, PassResolutionRequest> = ({ eventStore }: Dependencies) => async (request) => {
  const maybeResolution = await eventStore.load(request.id, Resolution);

  if (!maybeResolution) {
    throw new Error('Resolution not found');
  }

  const resolution = <Resolution> maybeResolution;

  resolution.pass();

  await eventStore.save(resolution);
};

export default passResolution;
