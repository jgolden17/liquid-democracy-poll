import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Resolution } from '../domain/Resolution';

export type TableResolutionRequest = {
  id: string;
  // TODO: i don't like "reason"
  reason: string;
};

export type TableResolutionUseCase = UseCase<TableResolutionRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const tableResolution: UseCaseFactory<Dependencies, TableResolutionRequest> = ({ eventStore }: Dependencies) => async (request) => {
  const maybeResolution = await eventStore.load(request.id, Resolution);

  if (!maybeResolution) {
    throw new Error('Resolution not found');
  }

  const resolution = <Resolution> maybeResolution;

  resolution.table(request.reason);

  await eventStore.save(resolution);
};

export default tableResolution;
