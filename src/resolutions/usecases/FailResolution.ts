import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Resolution } from '../domain/Resolution';

export type FailResolutionRequest = {
  id: string;
};

export type FailResolutionUseCase = UseCase<FailResolutionRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const failResolution: UseCaseFactory<Dependencies, FailResolutionRequest> = ({ eventStore }: Dependencies) => async (request) => {
  const maybeResolution = await eventStore.load(request.id, Resolution);

  if (!maybeResolution) {
    throw new Error('Resolution not found');
  }

  const resolution = <Resolution> maybeResolution;

  resolution.fail();

  await eventStore.save(resolution);
};

export default failResolution;
