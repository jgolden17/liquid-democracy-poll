import { UseCase, UseCaseFactory } from '../../core';
import { IResolutionRepository } from '../application/ResolutionRepository';
import { Resolution } from '../domain/Resolution';

export interface GetResolutionRequest {
  id: string;
  title?: string;
}

export type GetResolutionResponse = {
  id: string;
  title: string;
  proposal: string;
  proposedBy: string;
  status: string;
  tabledAt?: Date;
  tabledReason?: string;
  passedAt?: Date;
  failedAt?: Date;
};

export type GetResolutionUseCase = UseCase<GetResolutionRequest, GetResolutionResponse>;

type Dependencies = {
  resolutionRepository: IResolutionRepository;
}

const getResolution: UseCaseFactory<Dependencies, GetResolutionRequest, GetResolutionResponse> = (config: Dependencies) => {
  const { resolutionRepository } = config;

  return async (request) => {
    const maybeResolution = await resolutionRepository.findOne({ id: request.id });

    if (!maybeResolution) {
      throw new Error('Resolution not found');
    }

    const resolution = <Resolution> maybeResolution;

    return {
      id: resolution.id,
      title: resolution.title,
      proposal: resolution.proposal,
      proposedBy: resolution.proposedBy,
      status: resolution.status,
      tabledAt: resolution.tabledAt,
      tabledReason: resolution.tabledReason,
      passedAt: resolution.passedAt,
      failedAt: resolution.failedAt,
      comments: resolution.comments,
    };
  };
};

export default getResolution;
