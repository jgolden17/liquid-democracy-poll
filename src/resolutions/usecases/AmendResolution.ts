import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Resolution } from '../domain/Resolution';

export interface AmendResolutionRequest {
  id: string;
  title: string;
  proposal: string;
  amendedBy: string;
}

export type AmendResolutionUseCase = UseCase<AmendResolutionRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const amendResolution: UseCaseFactory<Dependencies, AmendResolutionRequest> = ({ eventStore }: Dependencies) => async request => {
  const resolutionAmendment = {
    title: request.title,
    proposal: request.proposal,
    amendedBy: request.amendedBy,
  };

  const maybeResolution = await eventStore.load(request.id, Resolution);

  if (!maybeResolution) {
    throw new Error('resolution not found');
  }

  const resolution = <Resolution>maybeResolution;

  resolution.amend(resolutionAmendment);

  await eventStore.save(resolution);
};

export default amendResolution;
