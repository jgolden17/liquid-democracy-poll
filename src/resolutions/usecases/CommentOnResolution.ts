import { v4 as uuid } from 'uuid';
import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Comment } from '../domain/Comment';
import { Resolution } from '../domain/Resolution';

export interface CommentOnResolutionRequest {
  id: string;
  comment: string;
  repliesTo?: string;
  commentedAt: Date;
  commentedBy: string;
}

export type CommentOnResolutionUseCase = UseCase<CommentOnResolutionRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const commentOnResolution: UseCaseFactory<Dependencies, CommentOnResolutionRequest> = ({ eventStore }: Dependencies) => async (request) => {
  const maybeResolution = await eventStore.load(request.id, Resolution);

  if (!maybeResolution) {
    throw new Error('resolution not found');
  }

  const resolution = <Resolution> maybeResolution;

  const comment = Comment.create({
    id: uuid(),
    body: request.comment,
    repliesTo: request.repliesTo,
    commentedAt: request.commentedAt,
    commentedBy: request.commentedBy,
  });

  resolution.addComment(comment);

  await eventStore.save(resolution);
};

export default commentOnResolution;
