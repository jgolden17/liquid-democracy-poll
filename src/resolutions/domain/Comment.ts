export interface IComment {
  id: string;
  repliesTo?: string;
  commentedBy: string;
  commentedAt: Date;
  body: string;
}

export class Comment implements IComment {
  id: string;
  repliesTo?: string;
  commentedBy: string;
  commentedAt: Date;
  body: string;

  private constructor(comment: IComment) {
    this.id = comment.id;
    this.repliesTo = comment.repliesTo;
    this.commentedBy = comment.commentedBy;
    this.commentedAt = comment.commentedAt;
    this.body = comment.body;
  }

  static create(props: IComment): IComment {
    if (!props.commentedAt) {
      throw new Error(`\`commented.commentAt\` is required but got ${props.commentedBy}`);
    }

    if (!props.commentedBy) {
      throw new Error(`\`commented.commentedBy\` is required by got ${props.commentedBy}`);
    }

    if (!props.body) {
      throw new Error(`\`comment.body\` is required by got ${props.body}`);
    }

    return new Comment(props);
  }
}
