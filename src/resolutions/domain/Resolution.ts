import { OptionalPick } from '../../core';
import { AggregateRoot, IAggregateRoot, IEvent } from '../../core/AggregateRoot';
import { withoutNulls } from '../../core/utils';
import { IComment } from './Comment';
import { ResolutionAmended, ResolutionCommentedOn, ResolutionFailed, ResolutionPassed, ResolutionProposed, ResolutionTabled } from './events';

export type ResolutionDetails = {
  id: string;
  title: string;
  proposal: string;
  proposedBy: string;
  status: string;
  tabledAt?: Date;
  tabledReason?: string;
  passedAt?: Date;
  failedAt?: Date;
  comments?: IComment[];
};

export interface IResolution extends IAggregateRoot {
  id: string;
  title: string;
  proposal: string;
  proposedBy: string;
  status: string;
  tabledAt?: Date;
  tabledReason?: string;
  comments: IComment[];
}

export type ResolutionAmendment = OptionalPick<
  ResolutionDetails,
  'title' | 'proposal'
>;

export class Resolution extends AggregateRoot implements IResolution {
  id: string;
  title!: string;
  proposal!: string;
  proposedBy!: string;
  status!: string;
  tabledAt?: Date;
  tabledReason?: string;
  passedAt?: Date;
  failedAt?: Date;
  comments: IComment[];

  constructor(id: string) {
    super(id);
    this.id = id;
    this.comments = [];
  }

  static create(resolutionDetails: Partial<ResolutionDetails>): Resolution {
    const { id } = resolutionDetails as { id: string };

    const resolution = new Resolution(id);

    resolution.apply(
      new ResolutionProposed(id, withoutNulls(resolutionDetails))
    );

    return resolution;
  }

  amend(amendment: ResolutionAmendment): void {
    this.apply(new ResolutionAmended(this.id, withoutNulls(amendment)));
  }

  table(reason: string): void {
    this.apply(new ResolutionTabled(this.id, { reason }));
  }

  pass(): void {
    this.apply(new ResolutionPassed(this.id));
  }

  fail(): void {
    this.apply(new ResolutionFailed(this.id));
  }

  addComment(comment: IComment): void {
    this.apply(new ResolutionCommentedOn(this.id, comment));
  }

  isTabled(): boolean {
    return this.status === 'TABLED';
  }

  hasPassed(): boolean {
    return this.status === 'PASSED';
  }

  hasFailed(): boolean {
    return this.status === 'FAILED';
  }

  loadFromSnapshot(snapshot: ResolutionDetails): void {
    Object.assign(this, snapshot);
  }

  when(event: IEvent): void {
    switch (event.type) {
      case 'RESOLUTION_PROPOSED': {
        const data = <ResolutionDetails>event.data;
        this.id = data.id;
        this.title = data.title;
        this.proposal = data.proposal;
        this.proposedBy = data.proposedBy;
        break;
      }
      case 'RESOLUTION_AMENDED': {
        const data = <ResolutionAmendment>event.data;

        if (data.title) {
          this.title = data.title;
        }

        if (data.proposal) {
          this.proposal = data.proposal;
        }

        break;
      }
      case 'RESOLUTION_TABLED': {
        this.tabledAt = event.timestamp;
        // TODO: gross
        this.tabledReason = (<ResolutionTabled> event).data.reason;
        this.status = 'TABLED';
        break;
      }
      case 'RESOLUTION_PASSED': {
        this.passedAt = event.timestamp;
        this.status = 'PASSED';
        break;
      }
      case 'RESOLUTION_FAILED': {
        this.failedAt = event.timestamp;
        this.status = 'FAILED';
        break;
      }
      case 'RESOLUTION_COMMENTED_ON': {
        console.log('RESOLUTION_COMMENTED_ON')
        this.comments.push(<IComment>event.data);
        break;
      }
      default:
        break;
    }
  }
}
