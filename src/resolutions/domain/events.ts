import { IEvent } from '../../core/AggregateRoot';

export class ResolutionProposed implements IEvent {
  public type = 'RESOLUTION_PROPOSED';

  constructor(
    public id: string,
    public data: unknown,
    public timestamp: Date = new Date()
  ) {}
}

export class ResolutionAmended {
  public type = 'RESOLUTION_AMENDED';

  constructor(
    public id: string,
    public data: unknown,
    public timestamp: Date = new Date()
  ) {}
}

export class ResolutionTabled {
  public type = 'RESOLUTION_TABLED';

  constructor(
    public id: string,
    public data: { reason: string },
    public timestamp: Date = new Date()
  ) {}
}

export class ResolutionPassed {
  public type = 'RESOLUTION_PASSED';

  constructor(
    public id: string,
    public data: unknown = null,
    public timestamp: Date = new Date()
  ) {}
}

export class ResolutionFailed {
  public type = 'RESOLUTION_FAILED';

  constructor(
    public id: string,
    public data: unknown = null,
    public timestamp: Date = new Date()
  ) {}
}

export class ResolutionCommentedOn implements IEvent {
  public type = 'RESOLUTION_COMMENTED_ON';

  constructor(
    public id: string,
    public data: unknown,
    public timestamp: Date = new Date()
  ) {}
}

