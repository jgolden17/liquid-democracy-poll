import PubSub from 'pubsub-js';
import { Resolution } from '../domain/Resolution';
import { IResolutionRepository } from './ResolutionRepository';

type SubscribersConfig = {
  resolutionRepository: IResolutionRepository;
};

const subscribers = ({ resolutionRepository }: SubscribersConfig): void => {
  PubSub.subscribe('RESOLUTION_PERSISTED', async (_: string, aggregate: Resolution) => {
    await resolutionRepository.save(aggregate);
  });
};

export default subscribers;
