import { Db, Document, Filter, ObjectId } from 'mongodb';
import { IRepository, Maybe } from '../../core';
import { Comment } from '../domain/Comment';
import { Resolution } from '../domain/Resolution';

export type ResolutionQuery = {
  id?: string;
  title?: string;
};

type Dependencies = {
  database: Db;
}

export type IResolutionRepository = IRepository<Resolution, ResolutionQuery>;

export class ResolutionRepository implements IResolutionRepository {
  private readonly collection = 'resolutions';

  private readonly database: Db;

  constructor({ database }: Dependencies) {
    this.database = database;
  }

  mapToAggregate(document: Document): Resolution {
    console.log('document', document);
    return Resolution.create({
      id: document._id.toHexString(),
      title: document.title,
      proposal: document.proposal,
      proposedBy: document.proposedBy,
      status: document.status,
      tabledAt: document.tabledAt,
      comments: document.comments.map((comment: Document) => Comment.create({
        id: comment._id.toHexString(),
        commentedBy: document.commentedBy,
        commentedAt: document.commentedAt,
        body: document.body,
      })),
    });
  }

  async find(): Promise<Resolution[]> {
    const documents = await this.database
      .collection(this.collection)
      .find()
      .toArray();

    return documents.map((document: Document) => this.mapToAggregate(document));
  }

  async findOne(query: ResolutionQuery): Promise<Maybe<Resolution>> {
    const { id, ...rest } = query;

    const filter: Filter<any> = rest;

    if (id) {
      filter._id = new ObjectId(id);
    }

    const document = await this.database
      .collection(this.collection)
      .findOne(filter);

    if (!document) {
      return null;
    }

    return this.mapToAggregate(document);
  }

  async save(resolution: Resolution): Promise<void> {
    await this.database
      .collection(this.collection)
      .findOneAndUpdate(
        { _id: new ObjectId(resolution.id) },
        {
          $set: {
            title: resolution.title,
            proposal: resolution.proposal,
            status: resolution.status,
            comments: resolution.comments,
          },
          $setOnInsert: {
            createdAt: new Date(),
          },
        },
        { upsert: true }
      );
  }
}
