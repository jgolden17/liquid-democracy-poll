import { asClass, asFunction, asValue } from 'awilix';
import { RegisterModule } from '../core';
import { ResolutionRepository } from './application/ResolutionRepository';
import subscribers from './application/subscribers';
import routesFactory from './interfaces/routes';
import AmendResolution from './usecases/AmendResolution';
import FailResolution from './usecases/FailResolution';
import GetResolution from './usecases/GetResolution';
import GetResolutions from './usecases/GetResolutions';
import PassResolution from './usecases/PassResolution';
import ProposeResolution from './usecases/ProposeResolution';
import TableResolution from './usecases/TableResolution';
import CommentOnResolution from './usecases/CommentOnResolution';

const register: RegisterModule = container => {
  // Providers
  container.register({
    resolutionRepository: asClass(ResolutionRepository),
  });

  // Use Cases
  container.register({
    proposeResolution: asFunction(ProposeResolution),
    amendResolution: asFunction(AmendResolution),
    failResolution: asFunction(FailResolution),
    passResolution: asFunction(PassResolution),
    tableResolution: asFunction(TableResolution),
    getResolution: asFunction(GetResolution),
    getResolutions: asFunction(GetResolutions),
    commentOnResolution: asFunction(CommentOnResolution),
  });

  container.build(routesFactory);
  container.build(subscribers);
};

export default register;
