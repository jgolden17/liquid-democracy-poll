import { Router } from 'express';
import { AmendResolutionUseCase } from '../usecases/AmendResolution';
import { ProposeResolutionUseCase } from '../usecases/ProposeResolution';

type RouterFactoryConfig = {
  router: Router;
  amendResolution: AmendResolutionUseCase;
  proposeResolution: ProposeResolutionUseCase;
};

type RouterFactory = (config: RouterFactoryConfig) => void;

const routesFactory: RouterFactory = ({
  router,
  amendResolution,
  proposeResolution,
}) => {
  router.post('/resolutions/propose', async (request, response) => {
    const resolutionId = await proposeResolution({
      title: request.body.title,
      proposal: request.body.proposal,
      proposedBy: request.body.user,
    });

    response.json({ id: resolutionId }).status(200);
  });

  router.post('/resolutions/amend', async (request, response) => {
    await amendResolution({
      id: request.body.id,
      title: request.body.title,
      proposal: request.body.proposal,
      amendedBy: request.body.user,
    });

    response.json({ message: 'success' }).status(200);
  });
};

export default routesFactory;
