import { createContainer, asValue, asClass } from 'awilix';
import Express from 'express';
import loadDatabase from './infrasctructure/database';
import { EventStore } from './infrasctructure/EventStore';
import resolutions from './resolutions';
import voters from './voters';
import { graphqlHTTP } from 'express-graphql';
import loadSchema from './graphql';

(async () => {
  const container = createContainer();

  const database = await loadDatabase();

  const schema = await loadSchema(container);

  const app = Express();

  app.use(Express.urlencoded({ extended: true }));
  app.use(Express.json());

  const router = Express.Router();

  app.use(router);

  app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true,
  }));

  container.register({
    database: asValue(database),
    eventStore: asClass(EventStore),
    router: asValue(router),
  });

  resolutions(container);
  voters(container);

  app.listen(3000);
})();
