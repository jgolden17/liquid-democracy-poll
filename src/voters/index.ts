import { asClass, asFunction } from 'awilix';
import { RegisterModule } from '../core';
import { VoterRepository } from './application/VoterRepository';
import routesFactory from './interfaces/routes';
import RegisterVoter from './usecases/RegisterVoter';
import GetVoter from './usecases/GetVoter';

const register: RegisterModule = container => {
  // Providers
  container.register({
    repository: asClass(VoterRepository),
  });

  // Use Cases
  container.register({
    getVoter: asFunction(GetVoter),
    registerVoter: asFunction(RegisterVoter),
  });

  container.build(routesFactory);
};

export default register;
