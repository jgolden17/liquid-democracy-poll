import { IAggregateRoot, AggregateRoot, IEvent } from '../../core/AggregateRoot';
import { VoterInfoChanged, VoterRegistered } from './events';

export type VoterDetails = {
  id: string;
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
  pronouns: string[];
  userName: string;
};

export interface IVoter extends IAggregateRoot {
  id: string;
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
  pronouns: string[];
  userName: string;
}
/**
 * I'm running into an issue with event sourcing user registration.
 *
 * The issue is - if someone changes their name or pronouns - I don't want
 * to keep a history of their previous entries.
 *
 * 1. What right do I have to that information?
 * 2. What happens if I need to rollback events?  I could potentially be
 *    exposing a user's deadname or previous pronouns.
 *
 * I could keep a seperate user collection that only includes names and pronouns
 * along with anything else that should be mutable/non-historical.
 *
 * The primary collection would contain the user id/username and anything I
 * want to keep a historical record of.
 */
export class Voter extends AggregateRoot implements IVoter {
  id!: string;
  userName!: string;
  fullName!: string;
  formallyAddressAs!: string;
  informallyAddressAs!: string;
  pronouns!: string[];

  constructor(id: string) {
    super(id);
  }

  static create({ id }: VoterDetails): Voter {
    return new Voter(id);
  }

  public register(voter: VoterDetails): void {
    this.fullName = voter.fullName;
    this.formallyAddressAs = voter.formallyAddressAs;
    this.informallyAddressAs = voter.informallyAddressAs;
    this.pronouns = voter.pronouns;

    this.apply(
      new VoterRegistered(voter.id, {
        userName: voter.userName,
      })
    );
  }

  public changeInfo(voter: Omit<VoterDetails, 'userName'>): void {
    this.fullName = voter.fullName;
    this.formallyAddressAs = voter.formallyAddressAs;
    this.informallyAddressAs = voter.informallyAddressAs;
    this.pronouns = voter.pronouns;

    this.apply(new VoterInfoChanged(voter.id));
  }

  public loadFromSnapshot(snapshot: VoterDetails): void {
    this.id = snapshot.id;
    this.fullName = snapshot.fullName;
    this.formallyAddressAs = snapshot.formallyAddressAs;
    this.informallyAddressAs = snapshot.informallyAddressAs;
    this.pronouns = snapshot.pronouns;
  }

  public when(event: IEvent): void {
    switch (event.type) {
      case 'VOTER_REGISTERED': {
        const data = <IVoter>event.data;
        this.userName = data.userName;
        break;
      }
      case 'VOTER_INFORMATION_CHANGED': {
        // nothing to see here. we do not store a users
        // previous entries.
        break;
      }
      default:
        break;
    }
  }
}
