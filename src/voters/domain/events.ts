import { IEvent } from '../../core/AggregateRoot';

export class VoterRegistered implements IEvent {
  public type = 'VOTER_REGISTERED';

  constructor(
    public id: string,
    public data: unknown,
    public timestamp: Date = new Date()
  ) {}
}

export class VoterInfoChanged implements IEvent {
  public type = 'VOTER_INFORMATION_CHANGED';

  constructor(
    public id: string,
    public data: unknown = {},
    public timestamp: Date = new Date()
  ) {}
}
