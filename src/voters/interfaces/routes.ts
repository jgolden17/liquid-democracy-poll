import { Router } from 'express';
import { RouterFactory } from '../../core';
import { GetVoterUseCase } from '../usecases/GetVoter';
import { RegisterVoterUseCase } from '../usecases/RegisterVoter';

type RouterFactoryConfig = {
  router: Router;
  getVoter: GetVoterUseCase;
  registerVoter: RegisterVoterUseCase;
};

const routesFactory: RouterFactory<RouterFactoryConfig> = ({
  router,
  getVoter,
  registerVoter,
}) => {
  router.get('/voters/:id', async (request, response) => {
    const voter = await getVoter({ id: request.params.id });

    response.json(voter).status(200);
  });

  router.post('/voters/register', async (request, response) => {
    const voterId = await registerVoter({
      fullName: request.body.fullName,
      formallyAddressAs: request.body.formallyAddressAs,
      informallyAddressAs: request.body.informallyAddressAs,
      pronouns: request.body.pronouns,
      userName: request.body.userName,
    });

    response.json({ id: voterId }).status(200);
  });
};

export default routesFactory;
