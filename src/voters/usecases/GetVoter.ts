import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Voter } from '../domain/Voter';

export type GetVoterRequest = {
  id: string;
};

export type GetVoterResponse = {
  id: string;
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
  pronouns: string[];
  userName: string;
};

export type GetVoterUseCase = UseCase<GetVoterRequest, GetVoterResponse>;

type Dependencies = {
  eventStore: IEventStore;
}

const getVoter: UseCaseFactory<Dependencies, GetVoterRequest, GetVoterResponse> = config => {
  const { eventStore } = config;

  return async request => {
    const maybeVoter = await eventStore.load(request.id, Voter);

    if (!maybeVoter) {
      throw new Error('Voter not found');
    }

    const voter = <Voter> maybeVoter;

    return {
      id: voter.id,
      fullName: voter.fullName,
      formallyAddressAs: voter.formallyAddressAs,
      informallyAddressAs: voter.informallyAddressAs,
      pronouns: voter.pronouns,
      userName: voter.userName,
    };
  };
};

export default getVoter;
