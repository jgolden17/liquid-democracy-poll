import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Voter } from '../domain/Voter';

export type ChangeVoterInformationRequest = {
  id: string;
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
  pronouns: string[];
};

export type ChangeVoterInformationUseCase = UseCase<ChangeVoterInformationRequest>;

type Dependencies = {
  eventStore: IEventStore;
}

const changeVoterInformation: UseCaseFactory<Dependencies, ChangeVoterInformationRequest> =
  config => {
    const { eventStore } = config;

    return async request => {
      const voterDetails = {
        id: request.id,
        fullName: request.fullName,
        formallyAddressAs: request.formallyAddressAs,
        informallyAddressAs: request.informallyAddressAs,
        pronouns: request.pronouns,
      };

      const maybeVoter = await eventStore.load(voterDetails.id, Voter);

      if (!maybeVoter) {
        throw new Error('Voter not found');
      }

      const voter = <Voter>maybeVoter;

      voter.changeInfo(voterDetails);

      // realising that this is going to create a new snapshot which is not
      // what I want.  The alternative would be to `{ overwriteSnapshot: true}`
      // but that may be an anti-pattern for an event store.
      // may need to persist the mutatble details directly to the read collection
      // instead
      await eventStore.save(voter, true);
    };
  };

export default changeVoterInformation;
