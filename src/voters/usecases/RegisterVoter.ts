import { ObjectId } from 'mongodb';
import { UseCase, UseCaseFactory } from '../../core';
import { IEventStore } from '../../infrasctructure/EventStore';
import { Voter } from '../domain/Voter';

export type RegisterVoterRequest = {
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
  pronouns: string[];
  userName: string;
};

export type RegisterVoterResponse = string;

export type RegisterVoterUseCase = UseCase<RegisterVoterRequest, RegisterVoterResponse>;

type Dependencies = {
  eventStore: IEventStore;
}

const RegisterVoter: UseCaseFactory<Dependencies, RegisterVoterRequest, RegisterVoterResponse> = (config: Dependencies) => {
  const { eventStore } = config;

  return async (request) => {
    const voterDetails = {
      // TODO: better way to create ids
      id: new ObjectId().toHexString(),
      fullName: request.fullName,
      formallyAddressAs: request.formallyAddressAs,
      informallyAddressAs: request.informallyAddressAs,
      pronouns: request.pronouns,
      userName: request.userName,
    };

    const voter = Voter.create(voterDetails);

    voter.register(voterDetails);

    // persist the "voter registered" event that contains only the usename of the user
    // create a snapshot with the users name and pronoun details
    // we do this because the snapsht will be overwritten if the user's name or pronouns
    // ever change, while the event data will only ever contain the username.
    // we do this so that we do not accidently deadname our users.
    await eventStore.save(voter, true);

    return voter.id;
  };
};

export default RegisterVoter;
