import { Db, Document, Filter, ObjectId } from 'mongodb';
import { IRepository, Maybe } from '../../core';
import { Voter } from '../domain/Voter';

type Name = {
  fullName: string;
  formallyAddressAs: string;
  informallyAddressAs: string;
};

export type VoterPersistanceModel = {
  id: string;
  userName: string;
  name: Name;
  pronouns: string[];
};

export type VoterQuery = {
  id?: string;
  name?: string;
  userName?: string;
};

export type IVoterRepository = IRepository<Voter, VoterQuery>;

export class VoterRepository implements IVoterRepository {
  private readonly database: Db;

  private readonly collection = 'voters';

  constructor({ database }: {database: Db}) {
    this.database = database;
  }

  private mapToAggregate(document: Document): Voter {
    return Voter.create({
      id: document._id.toHexString(),
      fullName: document.name.fullName,
      formallyAddressAs: document.name.formallyAddressAs,
      informallyAddressAs: document.name.informallyAddressAs,
      pronouns: document.pronouns,
      userName: document.userName,
    });
  }

  async find(): Promise<Voter[]> {
    const documents = await this.database
      .collection(this.collection)
      .find()
      .toArray();

    return documents.map((document: Document) => this.mapToAggregate(document));
  }

  async exits(id: string): Promise<boolean> {
    const document = await this.database
      .collection('voters')
      .findOne({ _id: new ObjectId(id) });

    return !!document;
  }

  async findOne(query: VoterQuery): Promise<Maybe<Voter>> {
    const { id, ...rest } = query;

    const filter: Filter<any> = rest;

    if (id) {
      filter._id = new ObjectId(id);
    }

    const document = await this.database
      .collection(this.collection)
      .findOne(filter);

    if (!document) {
      return null;
    }

    return this.mapToAggregate(document);
  }

  async save(voter: Voter): Promise<void> {
    const model: Omit<VoterPersistanceModel, 'id'> = {
      name: {
        fullName: voter.fullName,
        formallyAddressAs: voter.formallyAddressAs,
        informallyAddressAs: voter.informallyAddressAs,
      },
      pronouns: voter.pronouns,
      userName: voter.userName,
    };

    await this.database
      .collection(this.collection)
      .findOneAndUpdate(
        { _id: new ObjectId(voter.id) },
        { $set: model },
        { upsert: true }
      );
  }
}
