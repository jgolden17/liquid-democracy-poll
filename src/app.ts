import Express from 'express';

export default function getApplication(): [
  Express.Application,
  Express.Router
] {
  const app: Express.Application = Express();

  app.use(Express.urlencoded({ extended: true }));
  app.use(Express.json());

  const router = Express.Router();

  return [app, router];
}
