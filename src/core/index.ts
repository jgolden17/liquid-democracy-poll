import { AwilixContainer } from 'awilix';
import { Router } from 'express';

export type Maybe<T> = T | null;

export type IRouter = Router;

export interface IRepository<TAggregate, TQuery> {
  find(): Promise<TAggregate[]>;
  findOne(query: TQuery): Promise<Maybe<TAggregate>>;
  save(entity: TAggregate): Promise<void>;
}

export interface IRouterFactoryConfig {
  router: IRouter;
}

export type OptionalPick<T, K extends PropertyKey> = Pick<
  T,
  Extract<keyof T, K>
>;

export type RouterFactory<T extends {router: IRouter}> = (config: T) => void;

export type RegisterModule = (container: AwilixContainer) => void;

export type UseCase<TRequest, TResponse = void> = (request: TRequest) => Promise<TResponse>;

export type UseCaseFactory<TDependencies, TRequest, TResponse = void> = (config: TDependencies) => UseCase<TRequest, TResponse>;
