export interface IEvent {
  id: string;
  type: string;
  data: unknown;
  timestamp: Date;
}

export interface IAggregateRoot {
  id: string;
  version: number;
  history: IEvent[];
  changes: IEvent[];
}

export abstract class AggregateRoot implements IAggregateRoot {
  public id: string;

  public version = -1;

  private _changes: IEvent[];

  private _history: IEvent[];

  protected abstract when(event: IEvent): void;

  abstract loadFromSnapshot(data: unknown): void;

  constructor(id: string) {
    this.id = id;
    this._changes = [];
    this._history = [];
  }

  get changes(): IEvent[] {
    return this._changes;
  }

  get history(): IEvent[] {
    return this._history;
  }

  apply(event: IEvent): void {
    this.when(event);
    this.changes.push(event);
  }

  load(history: IEvent[]): void {
    this._history = history;

    history.forEach(event => {
      this.when(event);
      this.version += 1;
    });
  }

  clearChanges(): void {
    this._changes = [];
  }
}
