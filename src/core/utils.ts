// eslint-disable-next-line @typescript-eslint/ban-types
export const withoutNulls = (obj: object): object =>
  Object.entries(obj).reduce((collection, entry) => {
    const [key, value] = entry;

    if (!value) {
      return collection;
    }

    return {
      ...collection,
      [key]: value,
    };
  }, {});
