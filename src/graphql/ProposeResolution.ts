import { title } from "process";
import { InputType, Field, ObjectType, createUnionType } from "type-graphql";
import proposeResolution from "../resolutions/usecases/ProposeResolution";
import { Resolution } from "./Resolution";

@InputType({ description: 'Resolution details' })
export class ProposeResolutionInput {
  @Field(() => String)
  title!: string;

  @Field(() => String)
  proposal!: string;
}

@ObjectType({ description: 'Propose resolution success response' })
export class ProposeResolutionSuccess {
  @Field()
  public message!: string;

  @Field(() => Resolution)
  public resolution!: Partial<Resolution>;
}

@ObjectType({ description: 'Propose resolution error response' })
export class ProposeResolutionError {
  @Field()
  public message!: string;
}


export const ProposResolutionResult = createUnionType({
  name: "ProposResolutionResult",
  types: () => [ProposeResolutionSuccess, ProposeResolutionError] as const,
  resolveType: (value) => {
    if ("resolution" in value) {
      return ProposeResolutionSuccess;
    }

    return ProposeResolutionError;
  },
});
