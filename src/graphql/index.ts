import "reflect-metadata";
import { AwilixContainer } from "awilix";
import { GraphQLSchema } from 'graphql';
import { buildSchema } from "type-graphql";
import { ResolutionMutationsResolver, ResolutionQueriesResolver } from "./resolvers";

const loadSchema = async (container: AwilixContainer): Promise<GraphQLSchema> => {
  const schema = await buildSchema({
    resolvers: [
      ResolutionMutationsResolver,
      ResolutionQueriesResolver,
    ],
    container: {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      get: (someClass: any) => container.build(someClass),
    },
    emitSchemaFile: true,
  });

 return schema;
};

export default loadSchema;
