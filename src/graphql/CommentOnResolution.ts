import { InputType, Field, ObjectType, createUnionType } from "type-graphql";
import { Resolution } from "./Resolution";

@InputType({ description: 'Resolution details' })
export class CommentOnResolutionInput  {
  @Field()
  resolutionId!: string;

  @Field()
  comment!: string;

  @Field({ nullable: true })
  repliesTo?: string;
}

@ObjectType({ description: 'Comment on resolution success response' })
export class CommentOnResolutionSuccess {
  @Field()
  public message!: string;

  @Field(() => Resolution)
  public resolution!: Partial<Resolution>;
}

@ObjectType({ description: 'Comment on resolution error response' })
export class CommentOnResolutionError {
  @Field()
  public message!: string;

  @Field()
  public stack!: string;
}

export const CommentOnResolutionResult = createUnionType({
  name: "CommentOnResolutionResult",
  types: () => [CommentOnResolutionSuccess, CommentOnResolutionError] as const,
  resolveType: (value) => {
    if ("resolution" in value) {
      return CommentOnResolutionSuccess;
    }

    return CommentOnResolutionError;
  },
});
