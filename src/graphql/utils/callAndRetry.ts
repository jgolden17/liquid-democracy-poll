// eslint-disable-next-line @typescript-eslint/ban-types
function callAndRetry(fn: Function, maxAmountOfRetries = 10) {
  return async (...args: unknown[]) => {
    let error;

    for (let retries = 0; retries < maxAmountOfRetries; retries++) {
      console.log('retrying call', retries, args);
      try {
        return fn(...args);
      } catch (err) {
        error = err;
      }
    }

    throw error;
  };
}

export default callAndRetry;
