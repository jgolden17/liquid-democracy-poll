import { Mutation, Arg, Resolver } from "type-graphql";
import { AmendResolutionUseCase } from "../../resolutions/usecases/AmendResolution";
import { CommentOnResolutionUseCase } from "../../resolutions/usecases/CommentOnResolution";
import { GetResolutionUseCase } from "../../resolutions/usecases/GetResolution";
import { ProposeResolutionUseCase } from "../../resolutions/usecases/ProposeResolution";
import { AmendResolutionError, AmendResolutionInput, AmendResolutionResult, AmendResolutionSuccess } from "../AmendResolution";
import { CommentOnResolutionResult, CommentOnResolutionInput, CommentOnResolutionSuccess, CommentOnResolutionError } from "../CommentOnResolution";
import { ProposResolutionResult, ProposeResolutionInput, ProposeResolutionSuccess, ProposeResolutionError } from "../ProposeResolution";
import { Resolution } from "../Resolution";
import callAndRetry from "../utils/callAndRetry";

type Dependencies = {
  getResolution: GetResolutionUseCase;
  proposeResolution: ProposeResolutionUseCase;
  amendResolution: AmendResolutionUseCase;
  commentOnResolution: CommentOnResolutionUseCase;
}

@Resolver(() => Resolution)
export class ResolutionMutationsResolver {
  proposeResolutionUseCase: ProposeResolutionUseCase;
  getResolutionEventually: GetResolutionUseCase;
  amendResolutionUseCase: AmendResolutionUseCase;
  commentOnResolutionUseCase: CommentOnResolutionUseCase;

  constructor({ getResolution, proposeResolution, amendResolution, commentOnResolution }: Dependencies) {
    this.proposeResolutionUseCase = proposeResolution;
    this.amendResolutionUseCase = amendResolution;
    this.commentOnResolutionUseCase = commentOnResolution;
    this.getResolutionEventually = callAndRetry(getResolution, 5);
  }

  @Mutation(() => ProposResolutionResult, { description: 'Propose a new resolution' })
  async proposeResolution(@Arg("input") input: ProposeResolutionInput): Promise<ProposeResolutionSuccess | ProposeResolutionError> {
    try {
      const resolutionId = await this.proposeResolutionUseCase({
        title: input.title,
        proposal: input.proposal,
        proposedBy: 'Jonathan Golden',
      });

      const resolution = await this.getResolutionEventually({
        id: resolutionId,
      });

      return {
        message: 'Resolution proposed',
        resolution,
      } as ProposeResolutionSuccess;
    } catch (error) {
      return {
        message: error.message,
         error,
      } as ProposeResolutionError;
    }
  }

  @Mutation(() => AmendResolutionResult, { description: 'Amend a resolution' })
  async amendResolution(@Arg('input') input: AmendResolutionInput): Promise<AmendResolutionSuccess | AmendResolutionError> {
    try {
      await this.amendResolutionUseCase({
        id: input.resolutionId,
        title: input .title,
        proposal: input.proposal,
        amendedBy: 'Jonathan Golden',
      });

      const resolution = await this.getResolutionEventually({
        id: input.resolutionId,
      });

      return {
        message: 'Resolution amended',
        resolution,
      } as AmendResolutionSuccess;
    } catch (error) {
      return {
        message: error.message,
        stack: error.stack.toString(),
      } as AmendResolutionError;
    }
  }

  @Mutation(() => CommentOnResolutionResult, { description: 'Comment on a resolution' })
  async commentOnResolution(@Arg('input') input: CommentOnResolutionInput): Promise<CommentOnResolutionSuccess | CommentOnResolutionError> {
    try {
      await this.commentOnResolutionUseCase({
        id: input.resolutionId,
        comment: input.comment,
        commentedBy: 'Jonathan Golden',
        commentedAt: new Date(),
        repliesTo: input.repliesTo,
      });

      const resolution = await this.getResolutionEventually({
        id: input.resolutionId,
      });

      console.log('resolution', resolution);

      return {
        message: 'Comment added to resolution',
        resolution,
      } as CommentOnResolutionSuccess;
    } catch (error) {
      return {
        message: error.message,
        stack: error.stack.toString(),
      } as CommentOnResolutionError;
    }
  }
}
