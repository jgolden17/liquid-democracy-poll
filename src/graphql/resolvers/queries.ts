import "reflect-metadata";
import { Arg, Query, Resolver, ResolverInterface } from "type-graphql";
import { GetResolutionUseCase } from "../../resolutions/usecases/GetResolution";
import { GetResolutionsUseCase } from "../../resolutions/usecases/GetResolutions";
import { Resolution, ResolutionConnection, ResolutionEdge } from "../Resolution";

type Dependencies = {
  getResolution: GetResolutionUseCase;
  getResolutions: GetResolutionsUseCase;
}

@Resolver(() => Resolution)
export class ResolutionQueriesResolver implements ResolverInterface<Resolution> {
  getResolutionUseCase: GetResolutionUseCase;
  getResolutionsUseCase: GetResolutionsUseCase;

  constructor({ getResolution, getResolutions }: Dependencies) {
    this.getResolutionUseCase = getResolution;
    this.getResolutionsUseCase = getResolutions;
  }

  @Query(() => Resolution)
  async resolution(@Arg("id") id: string): Promise<Resolution | undefined> {
    const result = await this.getResolutionUseCase({ id });

    return result as Resolution;
  }

  @Query(() => ResolutionConnection, { description: 'Get a list of resolutions' })
  async resolutions(): Promise<ResolutionConnection> {
    const resolutions = await this.getResolutionsUseCase();

    return {
      nodes: resolutions as Resolution[],
      edges: resolutions.map((resolution) => ({ node: resolution } as ResolutionEdge)),
    };
  }

  proposedBy(): string {
    return 'Jonathan Golden ';
  }
}

