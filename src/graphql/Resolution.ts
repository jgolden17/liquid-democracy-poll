import { Field, ObjectType } from 'type-graphql';

@ObjectType({ description: 'Represents a comment on a resolution' })
export class Comment {
  @Field()
  id!: string;

  @Field()
  repliesTo?: string;

  @Field()
  commentedBy!: string;

  @Field()
  commentedAt!: string;

  @Field()
  body!: string;
}

@ObjectType({ description: 'Object representing a resolution' })
export class Resolution {
  @Field()
  id!: string;

  @Field()
  title!: string;

  @Field()
  proposal!: string;

  @Field()
  proposedBy!: string;

  @Field()
  status!: string;

  @Field()
  tabledAt!: Date;

  @Field()
  tabledReason!: string;

  @Field()
  passedAt!: Date;

  @Field()
  failedAt!: Date;

  @Field(() => [Comment])
  comments?: Comment[];
}

@ObjectType({ description: 'Object representing a resolution' })
export class ResolutionEdge {
  @Field()
  node!: Resolution;
}

@ObjectType({ description: 'Object representing a resolution' })
export class ResolutionConnection {
  @Field(() => [Resolution], { nullable: true })
  nodes!: Resolution[];

  @Field(() => [ResolutionEdge])
  edges!: ResolutionEdge[];
}

