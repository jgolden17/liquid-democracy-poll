import { InputType, Field, ObjectType, createUnionType } from "type-graphql";
import { Resolution } from "./Resolution";

@InputType({ description: 'Resolution details' })
export class AmendResolutionInput implements Partial<Resolution> {
  @Field()
  resolutionId!: string;

  @Field()
  title!: string;

  @Field()
  proposal!: string;
}

@ObjectType({ description: 'Amend resolution success response' })
export class AmendResolutionSuccess {
  @Field()
  public message!: string;

  @Field(() => Resolution)
  public resolution!: Partial<Resolution>;
}

@ObjectType({ description: 'Amend resolution error response' })
export class AmendResolutionError {
  @Field()
  public message!: string;

  @Field()
  public stack!: string;
}


export const AmendResolutionResult = createUnionType({
  name: "AmendResolutionResult",
  types: () => [AmendResolutionSuccess, AmendResolutionError] as const,
  resolveType: (value) => {
    if ("resolution" in value) {
      return AmendResolutionSuccess;
    }

    return AmendResolutionError;
  },
});
